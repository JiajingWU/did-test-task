(ns demo.body
  (:require
    [reagent.core :as r]))



(defn slick-body []
  [:div.slides_container
   [:div.slides
    [:div [:img {:src "img/scshot.jpg"}] [:p "Choose the machine type that’s right for your app – and  spin up in 55 seconds or less."]]
    [:div [:img {:src "img/scshot.jpg"}] [:p "Choose the machine type that’s right for your app – and  spin up in 55 seconds or less."]]
    [:div [:img {:src "img/scshot.jpg"}] [:p "Choose the machine type that’s right for your app – and  spin up in 55 seconds or less."]]
    [:div [:img {:src "img/scshot.jpg"}] [:p "Choose the machine type that’s right for your app – and  spin up in 55 seconds or less."]]
    [:div [:img {:src "img/scshot.jpg"}] [:p "Choose the machine type that’s right for your app – and  spin up in 55 seconds or less."]]
    ]
   ]
  )



(defn sign-form []
  [:div.box.signup-form
   [:h2.has-text-centered.has-text-weight-semibold "Deploy in seconds"]
   [:div.field.full-name
    [:div.control
     [:input.input.is-medium {:type "text" :placeholder "Full name"}]]]
   [:div.field
    [:div.control
     [:input.input.is-medium {:type "email" :placeholder "Email address"}]]]

   [:div.field
    [:div.control
     [:input.input.is-medium {:type "password" :placeholder "Create a password"}]]]

   [:div.field
    [:div.control
     [:button.button.is-medium.is-info.is-fullwidth "Create your account"]]]

   [:div.columns.is-mobile.is-vcentered
    [:div.column.is-one-third-desktop.is-one-quarters-tablet.is-one-quarters-mobile
     [:hr.hr]]

    [:span.column.is-one-third-desktop.has-text-centered.is-half-tablet.is-half-mobile
     "or sign up with"]
    [:div.column.is-one-third-desktop.is-one-quarters-tablet.is-one-quarters-mobile
     [:hr.hr]]]


   ; bottom btn
   [:div.columns.is-mobile
    [:div.column
     [:a.button.is-fullwidth.is-medium
      [:span.icon
       [:img {:src "https://img.icons8.com/color/48/000000/google-logo.png"}]]

      [:span "Google"]]]

    [:div.column
     [:a.button.is-fullwidth.is-medium
      [:span.icon
       [:i.fab.fa-github]]
      [:span "GitHub"]]]]

   [:div.term.has-text-centered
    [:span "By signing up you agree to the " [:a.term "Terms of Service"] "."]]])
(defn update-vals [m base]
  (reduce #(if (not= base %2) (assoc % %2 false) (assoc % %2 true)) m (keys m)))

(defn switch [click-status base]
  (swap! click-status update-vals base))

(defn s3-menu []
  (r/with-let [active? (r/atom {:0 true :1 false :2 false :3 false :4 false})]
              [:div.container--small.container
               [:div.s3-menu
                [:div.columns
                 [:ul.menu.column.menu-list.has-text-weight-regular
                  [:li {:on-click #(switch active? :0)
                        :class    (when (:0 @active?) :is-active)}
                   [:a.has-text-left "Deploy"]]
                  [:li {:on-click #(switch active? :1)
                        :class    (when (:1 @active?) :is-active)}
                   [:a.has-text-left "Scale"]]
                  [:li {:on-click #(switch active? :2)
                        :class    (when (:2 @active?) :is-active)}
                   [:a.has-text-left "Store"]]
                  [:li {:on-click #(switch active? :3)
                        :class    (when (:3 @active?) :is-active)}
                   [:a.has-text-left "Secure"]]
                  [:li {:on-click #(switch active? :4)
                        :class    (when (:4 @active?) :is-active)}
                   [:a.has-text-left "Monitor"]]]


                 [:div.column.has-text-left
                  [:div.menu-item
                   [:i.fab.fa-github]
                   [:p.s3-title "Kubernetes in minutes"]
                   [:p.has-text-primary "Spin up a managed Kubernetes cluster in just a few clicks. Simply specify the size and location of your worker nodes."]]

                  [:div.menu-item
                   [:i.fab.fa-github]
                   [:p.s3-title "Kubernetes in minutes"]
                   [:p.has-text-primary "Spin up a managed Kubernetes cluster in just a few clicks. Simply specify the size and location of your worker nodes."]]]


                 [:div.column.has-text-left
                  [:div.menu-item
                   [:i.fab.fa-github]
                   [:p.s3-title "Kubernetes in minutes"]
                   [:p.has-text-primary "Spin up a managed Kubernetes cluster in just a few clicks. Simply specify the size and location of your worker nodes."]]

                  [:div.menu-item
                   [:i.fab.fa-github]
                   [:p.s3-title "Kubernetes in minutes"]
                   [:p.has-text-primary "Spin up a managed Kubernetes cluster in just a few clicks. Simply specify the size and location of your worker nodes."]]]]]]))


; ======================================== section 1 ========================================

(defn s1 []
  [:section.hero.is-fullheight-with-navbar
   [:div.hero-body.s1
    [:div.container
     [:div.columns.is-vcentered
      [:div.column.is-6
       [:h1.title.s1-title.is-spaced.is-1 "Welcome to the developer cloud"]
       [:h2.s1-subtitle.is-4 "We make it simple to launch in the cloud and scale up as you grow—whether you’re running one virtual machine or ten thousand."]]

      [:div.column.is-1]
      [:div.column.is-5
       [sign-form]]]]]])



; ======================================== section 2 ========================================

(defn s2 []
  [:section.section.s2
   [:div.container.has-text-centered
    [:h2.has-text-weight-semibold "INNOVATIVE BUSINESSES TRUST DIGITALOCEAN"]
    [:div.Home-logoList.columns.is-vcentered.is-hidden-mobile.is-marginless
     [:div.Home-logoList-item.column
      [:img {:src "img/s2-img1.png"}]]
     [:div.Home-logoList-item.column
      [:img {:src "img/s2-img2.png"}]]
     [:div.Home-logoList-item.column
      [:img {:src "img/s2-img4.png"}]]
     [:div.Home-logoList-item.column
      [:img {:src "img/s2-img5.png"}]]
     [:div.Home-logoList-item.column
      [:img {:src "img/s2-img6.png"}]]
     [:div.Home-logoList-item.column
      [:img {:src "img/s2-img7.png"}]]]

    [:div.Home-logoList.columns.is-vcentered.is-hidden-tablet
     [:div.Home-logoList-item.column.columns.is-mobile.is-vcentered
      [:div.column [:img {:src "img/s2-img1.png"}]]
      [:div.column [:img {:src "img/s2-img2.png"}]]]
     [:div.Home-logoList-item.column.columns.is-mobile.is-vcentered
      [:div.column [:img {:src "img/s2-img1.png"}]]
      [:div.column [:img {:src "img/s2-img2.png"}]]]
     [:div.Home-logoList-item.column.columns.is-mobile.is-vcentered
      [:div.column [:img {:src "img/s2-img1.png"}]]
      [:div.column [:img {:src "img/s2-img2.png"}]]]]

    [:a.has-text-info.has-text-weight-medium "View customer stories >"]]])


; ======================================== section 3 ========================================

(defn s3 []
  [:div.hero.is-fullheight.s3
   [:div.hero-body
    [:div.container.has-text-centered
     [:p.title.is-spaced "Deploy and scale seamlessly"]
     [:p.subtitle "Our optimized configuration process saves your team time when running and scaling distributed applications, AI & machine learning workloads, hosted services, client websites, or CI/CD environments."]
     [s3-menu]]]])

; ======================================== section 4 ========================================
(defn s4 []
  [:div.hero.is-fullheight.s4
   [:div.hero-body
    [:div.container.has-text-centered
     [:p.title.is-spaced "Designed for developers"]
     [:p.subtitle "Build more and spend less time managing your infrastructure with our easy-to-use control panel and API."]
     [slick-body]]]])




(defn footer []
  [:footer.footer
   [:div.container
    [:div.columns.footer_list
     [:div.column.is-1]
     [:div.column [:p.has-text-white "Company"]
      [:ul
       [:li>a "About"]
       [:li>a "About"]
       [:li>a "About"]
       [:li>a "About"]
       [:li>a "About"]
       [:li>a "About"]]]


     [:div.column [:p.has-text-white "Products"]
      [:ul
       [:li>a "About"]
       [:li>a "About"]
       [:li>a "About"]
       [:li>a "About"]
       [:li>a "About"]
       [:li>a "About"]
       [:li>a "About"]
       [:li>a "About"]
       [:li>a "About"]
       [:li>a "About"]
       [:li>a "About"]
       [:li>a "About"]
       [:li>a "About"]
       [:li>a "About"]]]

     [:div.column [:p.has-text-white "Community"]
      [:ul
       [:li>a "About"]
       [:li>a "About"]
       [:li>a "About"]
       [:li>a "About"]
       [:li>a "About"]
       [:li>a "About"]
       [:li>a "About"]
       [:li>a "About"]]]

     [:div.column [:p.has-text-white "Solutions"]
      [:ul
       [:li>a "About"]
       [:li>a "About"]
       [:li>a "About"]
       [:li>a "About"]
       [:li>a "About"]
       [:li>a "About"]]]

     [:div.column [:p.has-text-white "Contact"]
      [:ul
       [:li>a "About"]
       [:li>a "About"]
       [:li>a "About"]
       [:li>a "About"]
       [:li>a "About"]
       [:li>a "About"]]]
     [:div.column.is-1]]
    [:hr.hr]
    [:div.columns

     [:div.column
      [:div.has-text-justified
       [:i.fab.fa-github.footer_logo]
       [:span.is-inline-block.logo_text "© 2019 DigitalOcean, LLC. All rights reserved."]]]


     [:div.column
      [:div.link.is-pulled-right
       [:a [:i.fab.fa-github]]
       [:a [:i.fab.fa-github]]
       [:a [:i.fab.fa-github]]
       [:a [:i.fab.fa-github]]
       [:a [:i.fab.fa-github]]
       [:a [:i.fab.fa-github]]]]]]])









(defn ^:export body [props]
  [:div
   [s1]
   [s2]
   [s3]
   [s4]
   [footer]])


